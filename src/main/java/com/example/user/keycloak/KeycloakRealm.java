package com.example.user.keycloak;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class KeycloakRealm {
    @Value("${keycloak.realm}")
    private String realmName;
    @Value("${keycloak.auth-server-url}")
    private String authServerUrl;
    @Value("${keycloak.resource}")
    private String resource;
    @Value("${keycloak.public-client}")
    private boolean publicClient;
    @Value("${keycloak.bearer-only}")
    private boolean bearerOnly;
    @Value("${keycloak.config.username}")
    private String userName;
    @Value("${keycloak.config.password}")
    private String password;
    @Value("client-id")
    private String clientId;
    @Value("client-secret")
    private String clientSecret;
    @Value("grant-type")
    private String grantType;
//    @Value("scopes")
//    private Set<String> scope;
//    @Value("token-uri")
//    private String tokenUri;
//    @Value("credentials")
//    private Map<String, Object> credentials;
//    @Value("user-info-uri")
//    private String userInfoUri;
}
