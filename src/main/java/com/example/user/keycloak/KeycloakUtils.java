package com.example.user.keycloak;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class KeycloakUtils {

    private final KeycloakRealm keycloakRealm;

    public KeycloakUtils(KeycloakRealm keycloakRealm) {
        this.keycloakRealm = keycloakRealm;
    }
    @Bean
    public Keycloak getKeycloakInstance() {
        Keycloak keycloak = Keycloak.getInstance(keycloakRealm.getAuthServerUrl(), keycloakRealm.getRealmName(),
                keycloakRealm.getUserName(), keycloakRealm.getPassword(), keycloakRealm.getResource());
        return keycloak;
//                env.getProperty("keycloak-config.client-secret"));
    }
    public List<String> getAuth(){
        List<String> list = new ArrayList<>();
        list.add(keycloakRealm.getAuthServerUrl());
        list.add(keycloakRealm.getAuthServerUrl());
        list.add(keycloakRealm.getAuthServerUrl());
        list.add(keycloakRealm.getAuthServerUrl());
        list.add(keycloakRealm.getAuthServerUrl());
        return list;
    }


}
