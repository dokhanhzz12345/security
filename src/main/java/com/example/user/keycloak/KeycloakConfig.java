package com.example.user.keycloak;

import lombok.AllArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.keycloak.OAuth2Constants.CLIENT_CREDENTIALS;

@Configuration
@AllArgsConstructor
public class KeycloakConfig {
    private final KeycloakRealm keycloakRealm;
    /**
     * @return Keycloak
     */
    @Bean(name = "keycloakCms")
    public Keycloak keycloakCms() {
        return KeycloakBuilder.builder()
//                .grantType(keycloakRealm.getGrantType())
                .serverUrl(keycloakRealm.getAuthServerUrl())
                .realm(keycloakRealm.getRealmName())
                .clientId(keycloakRealm.getClientId())
                .username(keycloakRealm.getUserName())
                .password(keycloakRealm.getPassword())
                .build();
    }
}
