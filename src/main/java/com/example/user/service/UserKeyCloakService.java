package com.example.user.service;

import com.example.user.keycloak.KeycloakRealm;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserKeyCloakService implements UserKeyCloakServiceImpl{
    private final Keycloak keycloak;
    private final KeycloakRealm keycloakRealm;

    public UserKeyCloakService(@Qualifier("keycloakCms") Keycloak keycloak, KeycloakRealm keycloakRealm) {
        this.keycloak = keycloak;
        this.keycloakRealm = keycloakRealm;
    }
    @Override
    public List<UserRepresentation> getListUserKeyCloak() {
        List<UserRepresentation> listUser = keycloak.realm(keycloakRealm.getRealmName()).users().list();
        return listUser;
    }
}
