package com.example.user.service;

import org.keycloak.representations.idm.UserRepresentation;

import java.util.List;

public interface UserKeyCloakServiceImpl {
    List<UserRepresentation> getListUserKeyCloak();
}
