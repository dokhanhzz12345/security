package com.example.user.controller;


import com.example.user.service.UserKeyCloakService;
import com.example.user.service.UserKeyCloakServiceImpl;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    private UserKeyCloakServiceImpl userKeyCloakService;

    @GetMapping("/userKeycloak")
    public List<UserRepresentation> getListUserKeyCloak() {
        return userKeyCloakService.getListUserKeyCloak();
    }
}
